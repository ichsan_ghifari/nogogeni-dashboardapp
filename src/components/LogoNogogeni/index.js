import React from 'react';
import {Image} from 'react-native';

import logoNogogeni from '../../assets/logoNogogeni.png';

const LogoNogogeni = () => {
  return <Image source={logoNogogeni}></Image>;
};

export default LogoNogogeni;
