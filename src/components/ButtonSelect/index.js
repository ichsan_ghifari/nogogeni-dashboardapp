import React from 'react';
import {StyleSheet, TouchableHighlight, Text} from 'react-native';
import DashBoardEVO from '../../pages/DashBoardEVO';

const ButtonSelect = ({onPress, txtSelect}) => {
  return (
    <TouchableHighlight style={styles.touch} onPress={onPress}>
      <Text style={styles.textSelect}>{txtSelect}</Text>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  touch: {
    backgroundColor: '#FF0000',
    width: 223,
    height: 40,
    borderRadius: 30,
    justifyContent: 'center',
    marginTop: 27,
  },
  textSelect: {
    color: '#ECEFEF',
    fontFamily: 'Jomhuria',
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
  },
});

export default ButtonSelect;
