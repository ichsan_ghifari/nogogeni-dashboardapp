import React from 'react';
import {View, ImageBackground, StyleSheet} from 'react-native';
import bgimage from '../../assets/Group3.png';

const ImageBG = () => {
  return (
    <View style={styles.container}>
      <ImageBackground source={bgimage} style={styles.image}></ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
});

export default ImageBG;
