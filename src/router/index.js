import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {SpalshScreen, SelectCar, DashBoardEVO} from '../pages';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="SpalshScreen">
      <Stack.Screen
        name="SpalshScreen"
        component={SpalshScreen}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SelectCar"
        component={SelectCar}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="DashBoardEVO"
        component={DashBoardEVO}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default Router;
