import React from 'react';
import {View, StyleSheet, Text, ImageBackground} from 'react-native';
import {ButtonSelect} from '../../components';
import bgimage from '../../assets/Group3.png';

const SelectCar = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ImageBackground source={bgimage} style={styles.image}>
        <View style={styles.pageWrapper}>
          <Text style={styles.text.selectCar}>SELECT CAR</Text>
          <ButtonSelect
            txtSelect={'NOGOGENI VI EVO'}
            onPress={() => navigation.navigate('DashBoardEVO')}
          />
          <ButtonSelect
            txtSelect={'NOGOGENI V'}
            onPress={() => navigation.navigate('DashBoardEVO')}
          />
          <ButtonSelect
            txtSelect={'NOGOGENI DS'}
            onPress={() => navigation.navigate('DashBoardEVO')}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    marginBottom: -50,
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  pageWrapper: {
    // backgroundColor: '#040341',
    width: 360,
    height: 600,
    paddingHorizontal: 57,
    paddingTop: 70,
    paddingBottom: 3,
    alignItems: 'center',
  },
  text: {
    selectCar: {
      marginTop: 50,
      color: '#ECEFEF',
      fontFamily: 'Jomhuria',
      fontWeight: '800',
      fontSize: 32,
      lineHeight: 45,
      textAlign: 'center',
    },
    txtButton: {},
  },
});

export default SelectCar;
