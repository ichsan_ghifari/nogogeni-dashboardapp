import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Button,
  ImageBackground,
} from 'react-native';
import {StopWatch} from '../../components';
import bgimage from '../../assets/Group3.png';

const axios = require('axios').default;

const DashboardEVO = () => {
  const [dataCar, setDataCar] = useState({
    A: '',
    V: '',
    W: '',
  });

  const getData = () => {
    axios
      .get(
        'https://platform.antares.id:8443/~/antares-cse/antares-id/tubitak/Tubitak/la',
        {
          headers: {
            'X-M2M-Origin': 'c4fb340ba1ba0975:932a36af19e6b7af',
            'Content-Type': 'application/json;ty=4',
            Accept: 'application/json',
          },
        },
      )
      .then(result => {
        let datacon = JSON.parse(result.data['m2m:cin'].con);
        setDataCar(datacon);
        console.log(dataCar);
      })
      .catch(err => console.log('err:', err));
  };

  let intervalId;

  const getDataInterval = () => {
    if (!intervalId) {
      intervalId = setInterval(getData, 1000);
    }
  };

  const stopGetDataInterval = () => {
    clearInterval(intervalId);
    // release our intervalID from the variable
    intervalId = null;
  };

  return (
    <View style={styles.container}>
      <ImageBackground source={bgimage} style={styles.image}>
        <ScrollView nestedScrollEnabled={true}>
          <View style={styles.pageWrapper}>
            <View style={styles.valueWrapper}>
              <Text style={styles.txtValue}>Voltage = {`${dataCar.V}`}</Text>
            </View>
            <View style={styles.valueWrapper}>
              <Text style={styles.txtValue}>Current = {`${dataCar.A}`} </Text>
            </View>
            <View style={styles.valueWrapper}>
              <Text style={styles.txtValue}>Power = {`${dataCar.W}`}</Text>
            </View>
            <Button title="GET DATA" onPress={getDataInterval} />
            <View style={styles.stopwatch}>
              <StopWatch />
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  pageWrapper: {
    // backgroundColor: '#040341',
    width: 360,
    height: 600,
    justifyContent: 'center',
    alignItems: 'center',
  },
  valueWrapper: {
    backgroundColor: '#FF0000',
    width: 221,
    height: 52,
    marginBottom: 32,
    paddingVertical: 12,
  },
  txtValue: {
    fontFamily: 'Jomhuria',
    color: '#ECEFEF',
    width: 221,
    height: 52,
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  stopwatch: {
    width: 221,
    height: 100,
    background: 'Transparant',
  },
});

export default DashboardEVO;
