import React, {useEffect} from 'react';
import {StyleSheet, View, Text, ImageBackground} from 'react-native';
import {LogoNogogeni} from '../../components';
import {SelectCar} from '../../pages/SelectCar';
import bgimage from '../../assets/Group3.png';

const SpalshScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('SelectCar');
    }, 5500);
  });
  return (
    <View style={styles.container}>
      <ImageBackground source={bgimage} style={styles.image}>
        <View style={styles.pageWrapper}>
          <View style={styles.logoWrapper}>
            <LogoNogogeni />
          </View>
          <View style={styles.textWrapper}>
            <Text style={styles.text.txtDashboard}>NOGOGENI DASHBOARD APP</Text>
          </View>
          <View style={styles}>
            <Text style={styles.text.copyRight}>
              copyright ©2021 EPS Nogogeni ITS
            </Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  image: {
    marginBottom: -50,
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  pageWrapper: {
    // backgroundColor: '#040341',
    width: 360,
    height: 600,
    paddingHorizontal: 57,
    paddingTop: 80,
    paddingBottom: 3,
    alignItems: 'center',
  },
  logoWrapper: {
    width: 10,
    height: 10,
    width: 250,
    height: 250,
    padding: 0,
  },
  textWrapper: {
    marginTop: 15,
    height: 90,
    width: 245,
  },
  text: {
    txtDashboard: {
      fontWeight: 'bold',
      fontSize: 25,
      textAlign: 'center',
      color: '#ECEFEFE5',
    },
    copyRight: {
      fontSize: 13,
      color: '#ECEFEFE5',
      textAlign: 'center',
      marginTop: 105,
      fontSize: 13,
      fontFamily: 'inter',
      fontWeight: 'bold',
      height: 16,
      width: 219,
    },
  },
});

export default SpalshScreen;
